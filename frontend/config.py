import os

port = int(os.getenv("PORT", 5000))
app_version = os.getenv("APP_VERSION", "0.1.0")
prometheus_port = int(os.getenv("PROMETHEUS_PORT", 8000))
backend_service = os.getenv("BACKEND_SERVICE", "http://localhost:5001")
