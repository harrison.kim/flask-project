import config
import json
import requests
from flask import Flask
from dotenv import load_dotenv
from prometheus_client import start_http_server, Summary, Info

app = Flask(__name__)

i = Info("build_version", "Description of info")
i.info({"version": config.app_version})
REQUEST_TIME = Summary(
    "http_request", "Time spent for http request", ["method", "endpoint"]
)
BASE_REQUEST_TIME = REQUEST_TIME.labels("GET", "/")
HELLO_REQUEST_TIME = REQUEST_TIME.labels("GET", "/hello")
TEST_REQUEST_TIME = REQUEST_TIME.labels("", "/test")


@app.route("/")
@BASE_REQUEST_TIME.time()
def default():
    return "<p>This is the default route</p>"


@app.route("/hello")
@HELLO_REQUEST_TIME.time()
def hello_world():
    return "<p>Hello, world! This is the /hello route</p>"


@app.route("/test")
@TEST_REQUEST_TIME.time()
def test():
    response = requests.get(config.backend_service)

    if response.status_code == 200:
        output = response.json()
        return f"received {output}", response.status_code

    return "ran into an error when calling backend", 404


@app.route("/<path:path>")
def catch_all(path):
    CATCH_ALL_REQUEST_TIME = REQUEST_TIME.labels("GET", f"/{path}")
    with CATCH_ALL_REQUEST_TIME.time():
        return f"Path {path} does not exist", 404


if __name__ == "__main__":
    print(f"Starting Prometheus on port {config.prometheus_port}")
    start_http_server(config.prometheus_port)
    load_dotenv()
    app.run(host="0.0.0.0", port=config.port)
