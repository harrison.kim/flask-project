# flask-project

Repo to hold fun Flask project stuff

## Running Locally
To run this project locally, a few libraries need to be installed via pip
- flask
- python-dotenv
- prometheus_client
- pytest
